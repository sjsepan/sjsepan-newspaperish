# Newspaperish Theme

Newspaperish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-newspaperish_code.png](./images/sjsepan-newspaperish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-newspaperish_codium.png](./images/sjsepan-newspaperish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-newspaperish_codedev.png](./images/sjsepan-newspaperish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-newspaperish_ads.png](./images/sjsepan-newspaperish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-newspaperish_theia.png](./images/sjsepan-newspaperish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-newspaperish_positron.png](./images/sjsepan-newspaperish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/9/2025
