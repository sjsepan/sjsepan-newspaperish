# Newspaperish Theme - Change Log

## [0.3.5]

- tweak widget/notification borders and backgrounds

## [0.3.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.3]

- update readme and screenshot

## [0.3.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- focusborder to match theme

## [0.3.1]

- fix manifest and pub WF

## [0.3.0]

- dimmer button BG so as to not compete visually with the project names in the Git sidebar
- fix manifest repo links
- retain v0.2.7 for those who prefer the earlier style

## [0.2.7]

- make lst act sel BG transparent
- make badge BG transparent

## [0.2.6]

- swap button.FG/BG

## [0.2.5]

- fix extension button FG/BG contrasts

## [0.2.4]

- adjust slider contrasts/transparencies

## [0.2.3]

- fix completion list contrast: editorSuggestWidget.selectedBackground, list.hoverBackground lightened

## [0.2.2]

- fix profile badge FG/BG contrast correctly

## [0.2.1]

- dim search placeholder FG
- add search border
- fix profile badge FG/BG contrast

## [0.2.0]

- retain v0.0.2 for those who prefer the previous style
- adjust contrasts

## [0.0.2]

- fix pkg screenshot url

## [0.0.1]

- Initial release
